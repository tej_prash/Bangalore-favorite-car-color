import numpy as np
import cv2 as cv
import os
from PIL import Image
import webcolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
#from skimage.feature import hog
from sklearn.svm import LinearSVC
#from obj_recog import get_main_color
kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE,(3,3))

path="traffic_data/"

def get_main_color(file,newimg):
    img = file
    #Returns (count,color-RGB)
    colors = img.getcolors(len(newimg)*len(newimg[0])) #put a higher value if there are many colors in your image
    max_occurence, most_present = 0, 0
    try:
        for c in colors:
            if c[0] > max_occurence:
                (max_occurence, most_present) = c
        return most_present
    except TypeError:
        raise Exception("Too many colors in the image")
    
def getmotion(x):
    cap = cv.VideoCapture(x)
    fgbg = cv.createBackgroundSubtractorMOG2()
    fgbg.setShadowValue(0)
    l=[]
    count=0
    try:
        while(1):
            
            count+=1
            ret, frame = cap.read()
            fgmask = fgbg.apply(frame)
            
            fmasked = cv.morphologyEx(fgmask, cv.MORPH_OPEN, kernel)

            if(count%100==0):
                m=[]
                m.append(frame)
                m.append(fmasked)
                l.append(m)
 
            
        cap.release()
        cv.destroyAllWindows()
        return l
    except:
        print("except")
        cap.release()
        cv.destroyAllWindows()
        return l

    
def minimize_contours(img,contours):
    contours=contours.tolist()
    for i in range(len(contours)):
        area = cv.contourArea(contours[i])
        if(area<25):
            contours
def nms(list_rect):
    x1=list_rect[:,0]
    y1=list_rect[:,1]
    x2=x1+list_rect[:,2]
    y2=y1+list_rect[:,3]
    
    non_overlap=[]
    area=(x2 - x1+1)*(y2-y1 +1)
    idxs=np.argsort(y2)     
    while len(idxs)>0:
        last_index=len(idxs)-1
        supress=[last_index]
        i=idxs[last_index]
        non_overlap.append(i)
        #Don't compare the last rectangle
        for index in range(last_index):
            j=idxs[index]

            #Comparing the upper left points
            xx1=max(x1[i],x1[j])
            yy1=max(y1[i],y1[j])

            #Comparing the bottom right points
            xx2=min(x2[i],x2[j])
            yy2=min(y2[i],y2[j])

            w=max(0,xx2-xx1+1)
            h=max(0,yy2-yy1+1)

            area_overlap=h*w
            if(area[i]+area[j]-area_overlap==0.00):
                ratio=0
            else:    
                ratio=float(area_overlap)/(area[i]+area[j]-area_overlap)

            #overlap threshold is 0.65
            if(ratio>0.2):
                supress.append(index)

        #delete all overlapping rectangles but preserve one of it(i is preserved)       
        idxs=np.delete(idxs,supress)
    final_rect=list_rect[non_overlap]
    return final_rect




def getcontours(y):
    ret, thresh = cv.threshold(y, 127, 255, 0)
    im2, contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
    
    return contours  



def addthevideo(file):
        count=0

        frame_count=0

        e=getmotion(file)
        print("len of e is",len(e))
        for i in range(len(e)): # for each frame
            print("frame",frame_count)
            frame_count+=1

            
            kernel = cv.getStructuringElement(cv.MORPH_OPEN,(7,7))
            orig=np.copy(e[i][0])

            blackandwhitecopy=np.copy(e[i][1])

            opening= cv.morphologyEx(e[i][1], cv.MORPH_CLOSE, kernel)
            contours=getcontours(opening)
            
            if(len(contours)==0):
                continue

            cpy_img=np.copy(e[i][0])

            not_so_clean_boxes=[]
            for i in range(len(contours)):
                x,y,w,h=cv.boundingRect(contours[i])
                if(cv.contourArea(contours[i])>200):
                    box=[x,y,w,h]

                    not_so_clean_boxes.append(box)

            not_so_clean_boxes=np.asarray(not_so_clean_boxes)
            if(len(not_so_clean_boxes)==0):
                continue
            clean_box=nms(not_so_clean_boxes)
            if(len(clean_box)==0):
                continue
            
            for i in clean_box:
                    cv.rectangle(cpy_img,(i[0],i[1]),(i[0]+i[2],i[1]+i[3]),(255,0,0),3)

            for i in clean_box:
                    cv.rectangle(blackandwhitecopy,(i[0],i[1]),(i[0]+i[2],i[1]+i[3]),(255,0,0),3)

            cv.imwrite("frames_with_boxes/frame"+str(frame_count)+".jpg",cpy_img)
            

           
            m=[]
  
            rgb_cars=[]
            path="traffic_data/"
            image=np.copy(orig)
            for i in clean_box:      
                    x=i[0]
                    y=i[1]
                    w=i[2]
                    h=i[3]
       
                    newimg=[]
                    for p in range(y,y+h):
                        l=[]
                        for q in range(x,x+w):
                            l.append(image[p][q])
                        
                        a=np.asarray(l)
                        
                        newimg.append(a)
                    newimg=np.asarray(newimg)
                    new64img=cv.resize(newimg,(64,64))
                    cv.imwrite(os.path.join(path,"car_"+file[:len(file)-5]+str(count)+".jpg"),new64img)
                    

                 
                    
                    count+=1
        print("done")





addthevideo("carshope.mp4")




        


