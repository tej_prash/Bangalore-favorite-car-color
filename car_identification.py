

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import glob
import os
import colorsys
import webcolors
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from functools import *
from PIL import Image

import keras
from keras.models import Sequential
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.core import Flatten, Dense, Activation, Reshape

keras.backend.set_image_dim_ordering('th')

def get_model():
    model = Sequential()
    
    model.add(Convolution2D(16, 3, 3,input_shape=(3,448,448),border_mode='same',subsample=(1,1)))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    # Layer 2
    model.add(Convolution2D(32,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2),border_mode='valid'))
    
    # Layer 3
    model.add(Convolution2D(64,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2),border_mode='valid'))
    
    # Layer 4
    model.add(Convolution2D(128,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2),border_mode='valid'))
    
    # Layer 5
    model.add(Convolution2D(256,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2),border_mode='valid'))
    
    # Layer 6
    model.add(Convolution2D(512,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2),border_mode='valid'))
    
    # Layer 7
    model.add(Convolution2D(1024,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    
    # Layer 8
    model.add(Convolution2D(1024,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    
    # Layer 9
    model.add(Convolution2D(1024,3,3 ,border_mode='same'))
    model.add(LeakyReLU(alpha=0.1))
    
    model.add(Flatten())
    
    # Layer 10
    model.add(Dense(256))
    
    # Layer 11
    model.add(Dense(4096))
    model.add(LeakyReLU(alpha=0.1))
    
    # Layer 12
    model.add(Dense(1470))
    
    return model




def crop_and_resize(image):
    return cv2.resize(image, (448,448))

def normalize(image):
    normalized = 2.0*image/255.0 - 1
    return normalized

def preprocess(image):
    cropped = crop_and_resize(image)
    normalized = normalize(cropped)
    # The model works on (channel, height, width) ordering of dimensions
    transposed = np.transpose(normalized, (2,0,1))
    return transposed




class Box:
    def __init__(self):
        self.x, self.y = float(), float()
        self.w, self.h = float(), float()
        self.c = float()
        self.prob = float()
        
def overlap(x1, w1, x2, w2):
    l1 = x1 - w1 / 2.
    l2 = x2 - w2 / 2.
    left = max(l1, l2)
    r1 = x1 + w1 / 2.
    r2 = x2 + w2 / 2.
    right = min(r1, r2)
    return right - left


def box_intersection(a, b):
    """

    :param a: Box 1
    :param b: Box 2
    :return: Intersection area of the 2 boxes
    """
    w = overlap(a.x, a.w, b.x, b.w)
    h = overlap(a.y, a.h, b.y, b.h)
    if w < 0 or h < 0:
        return 0
    area = w * h
    return area


def box_union(a, b):
    """

    :param a: Box 1
    :param b: Box 2
    :return: Area under the union of the 2 boxes
    """
    i = box_intersection(a, b)
    u = a.w * a.h + b.w * b.h - i
    return u


def box_iou(a, b):
    """

    :param a: Box 1
    :param b: Box 2
    :return: Intersection over union, which is ratio of intersection area to union area of the 2 boxes
    """
    return box_intersection(a, b) / box_union(a, b)



def yolo_output_to_car_boxes(yolo_output, threshold=0.2, sqrt=1.8, C=20, B=2, S=7):

    car_class_number = 6
   
    boxes = []
    SS = S*S  # number of grid cells
    prob_size = SS*C  # class probabilities
    conf_size = SS*B  # confidences for each grid cell

    probabilities = yolo_output[0:prob_size]
    confidence_scores = yolo_output[prob_size: (prob_size + conf_size)]
    cords = yolo_output[(prob_size + conf_size):]
    # Reshape the arrays so that its easier to loop over them
    probabilities = probabilities.reshape((SS, C))
    confs = confidence_scores.reshape((SS, B))
    cords = cords.reshape((SS, B, 4))
    for grid in range(SS):
        for b in range(B):
            bx = Box()

            bx.c = confs[grid, b]

            # bounding box xand y coordinates are offsets of a particular grid cell location,
            # so they are also bounded between 0 and 1.
            # convert them absolute locations relative to the image size
            bx.x = (cords[grid, b, 0] + grid % S) / S
            bx.y = (cords[grid, b, 1] + grid // S) / S


            bx.w = cords[grid, b, 2] ** sqrt
            bx.h = cords[grid, b, 3] ** sqrt

            # multiply confidence scores with class probabilities to get class sepcific confidence scores
            p = probabilities[grid, :] * bx.c

            # Check if the confidence score for class 'car' is greater than the threshold
            if p[car_class_number] >= threshold:
                bx.prob = p[car_class_number]
                boxes.append(bx)

    # combine boxes that are overlap

    # sort the boxes by confidence score, in the descending order
    boxes.sort(key=lambda b: b.prob, reverse=True)


    for i in range(len(boxes)):
        boxi = boxes[i]
        if boxi.prob == 0:
            continue

        for j in range(i + 1, len(boxes)):
            boxj = boxes[j]

            # If boxes have more than 40% overlap then retain the box with the highest confidence score
            if box_iou(boxi, boxj) >= 0.4:
                boxes[j].prob = 0

    boxes = [b for b in boxes if b.prob > 0]

    return boxes
path=""

def centroid_histogram(clt):
	# grab the number of different clusters and create a histogram
	# based on the number of pixels assigned to each cluster
	numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
	(hist, _) = np.histogram(clt.labels_, bins = numLabels)
 
	# normalize the histogram, such that it sums to one
	hist = hist.astype("float")
	hist /= hist.sum()
	# return the histogram
	return hist

#Uses KMeans clustering to clustering shades of colors into primary colors
def cluster_shades(colors_dominant,clusters):
    clf=KMeans(n_clusters=clusters)
    clf.fit(colors_dominant)
    centers=clf.cluster_centers_
    centers=centers.tolist()

    for i in range(len(centers)):
        centers[i]=(int(centers[i][0]),int(centers[i][1]),int(centers[i][2]))
    labels=clf.labels_
    labels=labels.tolist()
    return (centers,labels) 

#Using CIE LAB space to cluster colors
def modified_cluster_shades(colors_dominant,clusters):
    colors=[[[list(i)[::-1]]] for i in colors_dominant]#Converts rgb to bgr
    colors=[np.uint8(np.asarray(i)) for i in colors]
    colors_lab=[cv2.cvtColor(i,cv2.COLOR_BGR2Lab) for i in colors]
    
    colors_ab=[list((i[0][0][1],i[0][0][2])) for i in colors_lab]
    colors_l=[i[0][0][0] for i in colors_lab]
    clf=KMeans(n_clusters=clusters)
    clf.fit(colors_ab)

    centers_lab=[list((90,int(i[0]),int(i[1]))) for i in clf.cluster_centers_]
    colors=[[[list(i)]] for i in centers_lab]#Converts rgb to bgr

    colors=[np.uint8(np.asarray(i)) for i in colors]
    colors_rgb=[cv2.cvtColor(i,cv2.COLOR_Lab2RGB) for i in colors]
    colors_rgb=[i[0][0].tolist() for i in colors_rgb]
    return colors_rgb,clf.labels_.tolist()

    
def get_main_color(file,newimg):
    image = cv2.imread(file)
    #Returns (count,color-RGB)
    #colors_old = img.getcolors(len(newimg)*len(newimg[0])) #put a higher value if there are many colors in your image
    #max_occurence, most_present = 0, 0
    try:
        #image=cv2.imread("IMG_5851.JPG")
        image=cv2.resize(image,(128,128))
        image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #cv2.imshow("ima",image)
        #cv2.waitKey(5)
        #print(image)
        image=image.reshape((image.shape[0]*image.shape[1],3))
        clt=KMeans(n_clusters=3)
        clt.fit(image)
        #print(clt.cluster_centers_)

        hist=centroid_histogram(clt)
        dom_color=max(hist)
        

        max_color=np.argmax(hist)
        final_color=clt.cluster_centers_[max_color]
        final_color=[int(i) for i in final_color]
        final_color=np.asarray([[np.uint8(np.asarray(final_color))]])
        final_color_hsv=cv2.cvtColor(final_color,cv2.COLOR_RGB2HSV)
        
        if(final_color_hsv[0][0][2]>20):
            #print("no black detected")
            return final_color[0][0].tolist()
        
        maximum=0
        second_color_max=0
        for i in range(len(hist)):
                if(i!=max_color):
                        if(hist[i]>maximum):
                                maximum=hist[i]
                                second_color_max=i

        #print("largest",hist[max_color],clt.cluster_centers_[max_color],"second largest",clt.cluster_centers_[second_color_max],hist[second_color_max])
        #print(second_color_max)
        final_color=[int(i) for i in clt.cluster_centers_[second_color_max]]
        #print(final_color,clt.cluster_centers_[max_color])
        return final_color

    except TypeError:
        raise Exception("Too many colors in the image")
        
def draw_boxes(boxes,im, crop_dim):
    
    imgcv1 = im.copy()
    [xmin, xmax] = crop_dim[0]
    [ymin, ymax] = crop_dim[1]
    #print("boxes length",len(boxes))
    color=[]
    height, width, _ = imgcv1.shape
    for b in boxes:
        w = xmax - xmin
        h = ymax - ymin

        left  = int ((b.x - b.w/2.) * w) + xmin
        right = int ((b.x + b.w/2.) * w) + xmin
        top   = int ((b.y - b.h/2.) * h) + ymin
        bot   = int ((b.y + b.h/2.) * h) + ymin

        if left  < 0:
            left = 0
        if right > width - 1:
            right = width - 1
        if top < 0:
            top = 0
        if bot>height - 1: 
            bot = height - 1
        
        thick = 5 #int((height + width // 150))
        
        cv2.rectangle(imgcv1, (left, top), (right, bot), (255,0,0), thick)

        #converting rgb to bgr
        im=im[:,:,::-1]

        #Cropping the image
        img=im[top:bot,left:right,:]
        cv2.imwrite("img.jpg",img)
        color.append(get_main_color(os.path.join(path,"img.jpg"),img))
        
    return imgcv1,color



# Load weights
from utils import load_weights

model = get_model()
load_weights(model,'yolo-tiny.weights')

fp=open('colors_final_test.txt','w')
fp2=open('colors_with_files_final_test.txt','w')

#Use a color naming library to labels the colors obtained by kmeans
def color_naming(center):
    try:
                actual_color=webcolors.rgb_to_name(tuple(center))
                color_names=str(actual_color)
    except ValueError:
                d={}
                for (key,name_color) in webcolors.css3_hex_to_names.items():
                    r_w,g_w,b_w=webcolors.hex_to_rgb(key)
                    r,g,b=(int(center[0]),int(center[1]),int(center[2]))
                    #calculates hsv values
                    lib_lab=np.uint8(np.asarray([[list((r_w,g_w,b_w))]]))
                    lib_lab=cv2.cvtColor(lib_lab,cv2.COLOR_RGB2Lab)

                    given_lab=np.uint8(np.asarray([[list((r,g,b))]]))
                    given_lab=cv2.cvtColor(given_lab,cv2.COLOR_RGB2Lab)

                    #Extracting individual l,a,b values
                    r_w,g_w,b_w=((lib_lab[0][0])[0],(lib_lab[0][0])[1],(lib_lab[0][0])[2])
                    r,g,b=((given_lab[0][0])[0],(given_lab[0][0])[1],(given_lab[0][0])[2])
                    #calculate mean squared error  
                    g=(int(g_w)-int(g))**2
                    b=(int(b_w)-int(b))**2
                    d[r+g+b]=name_color
                #print(d)
                color_names=d[min(d.keys())]
    return color_names



# In[57]:
def find_colors_images(path):
    lab_colors=[]
    rgb_colors=[]
    image_names = glob.glob(path + '/**/*.jpg', recursive=True)
    print(len(image_names))
    image_names.extend(glob.glob(path + '/**/*.png', recursive=True))
    for image in image_names:
        test_image = mpimg.imread(image)
        pre_processed = preprocess(test_image)
        batch = np.expand_dims(pre_processed, axis=0)
        batch_output = model.predict(batch)


        fp2.write(image.split('\\')[::-1][0]+" ")
        boxes = yolo_output_to_car_boxes(batch_output[0], threshold=0.2)
        final,colors= draw_boxes(boxes, test_image, ((0,test_image.shape[1]),(0,test_image.shape[0])))

        final=cv2.cvtColor(final,cv2.COLOR_RGB2BGR)
        cv2.imwrite("final.jpg",final)
        rgb_colors.extend(colors)
        
        #Writing into a reference file
        for color in colors:
            fp2.write(color_naming(color)+" ")
        fp2.write("\n")
        
        #Convert rgb colors to hsv colors
        colors=[[[list(i)[::-1]]] for i in colors]#Converts rgb to bgr
        print(colors)
        colors=[np.uint8(np.asarray(i)) for i in colors]
        colors=[cv2.cvtColor(i,cv2.COLOR_BGR2Lab) for i in colors]
        
        colors=[i[0][0] for i in colors]

        
        lab_colors.extend(colors)
 
        
    #decides the numberof clusters
    centers,labels=modified_cluster_shades(rgb_colors,6)
    color_names=[]
    for i in range(len(centers)):
        print(centers[i])
        color_names.append(color_naming(centers[i]))
    for i in range(len(labels)):
        labels[i]=color_names[labels[i]]
        #print(labels[i])
        fp.write(str(labels[i])+" ")
    

try:
    find_colors_images("traffic_data/")
    fp.close()
    fp2.close()
except:
    print("exception\n")
    fp.close()
    fp2.close()
