Instructions for execution:

1) extractmovingobjects_fromvideo.py:
Takes the sample video "carshope.mp4" and uses BackgroundSubtractor and contour approximation in order to obtain the cropped images of moving objects. These are placed in the folder "/traffic_data"
The bounding boxes obtained for each frame are placed in the order "/frames_with_boxes"

2) car_identification.py:
Takes all images present in "/traffic_data", along with the cropped images from the video frames, and applies YOLO to it, followed by color detection on each detected car.

Output:
For a test run, the following results were obtained:
The text file "colors_with_files_final_3_13.txt" contains the name of the images followed by the colours identified. 
The text file "colors_final_3_13.txt" contains space separated names of colors after applying clustering to the dominant colors
"hadoop_output.txt" contains the number of cars of each color

Details:
The model developed consists of the following pipeline:
Traffic images->YOLO(Convolutional neural network) -> Bounding boxes around cars ->
Dominant color detection in each bounding box -> K-means clustering of all dominant colors
-> WordCount of each color
Traffic videos->BackgroundSubtractor to recognize moving objects -> Finding contours of
the moving objects -> Eliminating small sized objects -> Feeding cropped output images of
cars to YOLO -> Bounding boxes around cars -> Dominant color detection in each bounding
box -> K-means clustering of all dominant colors -> WordCount of each color
Phase 1:
Firstly, the above mentioned model supports both videos as well as still images. ​Given a
dataset of images of traffic, the model reads all images of traffic from the given path. This is
then passed to YOLO(You look only once) object detector. YOLO is a powerful neural
network that is capable for detecting various kinds of objects. The weights file used for the
network is open source and our model uses this set of pre-trained weights. The output of the
model is 1470 dimensional vector, which contains coordinates and each bounding box along
with its confidence. These two parameters are used to choose bounding boxes whose
confidence level is greater than 0.2.

Secondly, the color of each car in a given bounding box is extracted using two simple rules-
1) Find the 3 most dominant colors in the given image using KMeans clustering.

a) If the dominant color is black, return the second most dominant color. This is
because the black window tints of cars could be detected as the most
dominant
b) If the dominant color is not black, return the dominant color

Thereafter, the model applies KMeans clustering to the list of all dominant colors obtained by
first transforming the RGB values to CIE La*b values. This is because RGB color space
yields poor results when calculating Euclidean distance compared to the Euclidean distance
calculated using only the a(green-red space) and b(blue-yellow space) values. The result
obtained is a list of 6 centers,consisting of RGB values.
In order to extract names of colors, RGB values are once again transformed to LAB color
space, in order to locate the closest color from the standard set of colors using euclidean
distances. A color naming library is used to get the names of colors from the approximated
LAB values. Each color name is written into a text file and all names are separated by white
spaces.